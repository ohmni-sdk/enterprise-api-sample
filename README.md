## Introduction

Enterprise API is a set of RESTful web services for application developers to interact and manage their Ohmni robot fleets.

* User & Robot API: to programmatically list and manage all users and robots in the fleet.
* Data API: to query the usage data of the users and robots.
* Share Link API: to manage the schedule of your robots.
* Robot Action API: to interact with the specific robot (reboot, request upgrade, set home screen, load web page, set overlay HTML).

## Authentication
In order to send any RESTful request to the API endpoint, users need to acquire the API key for authentication. As the super-admin of the enterprise, users can login to the Enterprise Portal settings page and generate an API key from there. Users also can revoke/delete the unused keys.

To make an authenticated request to the API using your API key, the application can use HTTP Basic authentication. The request would look something like this:

  curl --request GET \
  --url 'https://enterprise-api.ohmnilabs.com/1.0/<API_END_POINTS>' \
  --api_key 'anystring:<YOUR_API_KEY>'

## API References
http://enterprise-docs.ohmnilabs.com/

## API Rate Limit
* API Throttle Limits: 100 rps per Enterprise account. If exceeding 100 rpm, application will experience active performance delay to the request. If the total request per second is too large (more than 150 rps), those exceeded request will receive “Too Many Requests” error (HTTP 429 code).

### Sample:
The sample code (written in Javascript) for using Enterprise API can be tried here:

* [https://ohmni-sdk.gitlab.io/enterprise-api-sample/](https://ohmni-sdk.gitlab.io/enterprise-api-sample/)

Please refer to the index.html for the code. Please note that we do not recommended to use this method in your system (it’s better to use server side code to call our Enterprise API in order to protect your API Key).
